// Copyright (c) 2021, bhavin and contributors
// For license information, please see license.txt


function get_sop_names(frm){

    var sop_names=["Select SOP"]
    frappe.db.get_doc('Area Type Master', frm.doc.area_type)
    .then(doc => {
        console.log(doc)
        var sop_list=doc.add_sop

        for (var i = 0; i < sop_list.length; i++) {
            sop_names.push(sop_list[i].sop_name)
        	}
        frm.set_df_property('sop_name', 'options', sop_names);
		if(frm.doc.sop_name==undefined){frm.set_value("sop_name","Select SOP")}
		else{frm.refresh_field("sop_name")}        
    })
}

function get_task_names(frm){
    console.log("get task called")
    function waitForElement(){
    
    if(typeof frm.doc.sop_name !== "undefined"){
    var task_names=["Select Task"]
    console.log("defined sop_name=",frm.doc.sop_name)
    if(frm.doc.sop_name!="Select SOP"){
    
    frappe.db.get_doc('SOP Master', frm.doc.sop_name)
    .then(doc => {
        console.log("SOP Fetch",doc)
        var task_list=doc.add_task

        for (var i = 0; i < task_list.length; i++) {
            task_names.push(task_list[i].task_title)
        	}
        frm.set_df_property('task_name', 'options', task_names);
		if(frm.doc.task_name==undefined){frm.set_value("task_name","Select Task")}
		else{frm.refresh_field("task_name")}        
    })
    }
    else{
        console.log("running else",frm.doc.task_name,task_names)
        // if(frm.doc.task_name==undefined)||{
            frm.set_df_property('task_name', 'options', task_names);
            frm.set_value("task_name","Select Task")
            frm.refresh_field("task_name")
        // }  
    }
    return;
    }
    else{
        console.log("waiting for elemnt");
        setTimeout(waitForElement, 250);
        }
    }

    waitForElement();
}

frappe.ui.form.on('Allocate Organization', {

	before_load: function(frm) {
		if(frm.doc.area_type!=undefined){
		get_sop_names(frm);
		get_task_names(frm);
	}},
	sop_name:function(frm){
		
		console.log("selected function called",frm.doc.sop_name);
		get_task_names(frm);
		

	}
	
});
