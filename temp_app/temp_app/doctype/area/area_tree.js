var options_list;
var default_project="Project 1"

// function waitForElement(var_name){
// if(typeof var_name !== "undefined"){
//     console.log("options avalibe")
//     return;
// }
// else{
//     setTimeout(waitForElement, 250);
//     }
// }


function waiting_fuction(){
	console.log("waiting_fuction");
}

function get_sop_fields(node,dlg){
    var sop_fields=[]
    frappe.db.get_doc('Area', node)
    .then(doc => {
        console.log(doc)

        var sop_list=doc.add_sop

        for (var i = 0; i < sop_list.length; i++) {
            dlg.fields.push(
            {
                label: sop_list[i].sop_name,
                fieldname: i,
                fieldtype: 'Button',
                // sop_list[i]
            })
        }
        let sop_pop = new frappe.ui.Dialog(dlg);
        sop_pop.show();
    })
}


frappe.provide("frappe.treeview_settings");
frappe.treeview_settings["Area"] = {
    get_tree_root: false,
		filters: [
		{
			fieldname: "project_name",
			fieldtype:"Select",
			options: ["Project 1"],
			label: __("project_options"),
			default: default_project


		}],
    post_render: function(treeview) {
        console.log("post_render")
    },
    refresh:function(treeview){
        console.log("refresh");
    },
    onload: function(treeview){
    	

    	var count=0;
    	var x
    	while(true){
    		if(x==undefined)
    		{
    			x=document.getElementsByTagName("select");
    			setTimeout(waiting_fuction,1000);
    			console.log(count);
    			count++;
    		}
    		else{
    			break;
    		}
    	}
    	x[0].onchange=function(){
			console.log("changed value")
		}

		frappe.call({
    		method: "temp_app.temp_app.doctype.area_master.area_master.get_projects",
    		callback:function(r){
        	console.log("inside call",r.message)
        	options_list=r.message
    		
    	options_list.forEach(add_option);


    	function add_option(item, index) {
            // console.log(item,!in_list(options_list,item))
            if(item!=default_project){
  			var element = document.createElement("option");
  			console.log("item=",item)
  			element.setAttribute("value", item);
  			element.innerText=item;
  			x[0].appendChild(element);
			}
            }
		document.getElementsByClassName("placeholder ellipsis text-extra-muted xs")[0].style="display: none;"
		}
	})  
    	console.log("onload");
   },
   toolbar: [
        {
            label:__("Option"),
            condition: function(node) {
                return true;
            },
            click: function(node) {
                console.log(node.title);
                
                let d = new frappe.ui.Dialog({
                title: 'Options',
                fields: [
                   // {
                   //      label: 'SOP',
                   //      fieldname: 'sop',
                   //      fieldtype: 'Button',
                   //      click:function(){
                   //          d.hide();
                   //          // console.log("add SOP");
                   //          var dlg={
                   //          title: 'SOP List',
                   //          fields:[
                   //             {
                   //                  label: '+ Add SOP',
                   //                  fieldname: 'status',
                   //                  fieldtype: 'Button',
                   //                  click:function(){
                   //                      frappe.set_route("Form","Area",node.title)
                   //                  }
                   //              },

                   //          ],
                   //          primary_action_label: 'Cancle',
                   //          primary_action(values) {
                   //              console.log(values);
                   //              sop_pop.hide();
                   //              }
                   //          };

                   //          get_sop_fields(node.title,dlg);


                   //      }
                   //  },
                    {
                       "fieldname": "column_break_1",
                       "fieldtype": "Column Break"
                    },

                    {
                        label: 'Start',
                        fieldname: 'start',
                        fieldtype: 'Button',
                        click:function(){
                            console.log("start area");
                        }
                    },
                    {
                       "fieldname": "column_break_2",
                       "fieldtype": "Column Break"
                    },                    
                    {
                        label: 'Pause',
                        fieldname: 'pause',
                        fieldtype: 'Button',
                        click:function(){
                            console.log("pause area");
                        }
                    },
                    {
                       "fieldname": "column_break_3",
                       "fieldtype": "Column Break"
                    },
                    {
                        label: 'Stop',
                        fieldname: 'stop',
                        fieldtype: 'Button',
                        click:function(){
                            console.log("stop area");
                        }
                    },
                    {
                        "fieldname": "section_break_1",
                        "fieldtype": "Section Break"
                    },
                   {
                        label: 'Allocate organization',
                        fieldname: 'allocate_organization',
                        fieldtype: 'Button',
                        click:function(){
                            frappe.db.get_doc('Area', node.label)
                            .then(doc => {
                                frappe.route_options = {
                                "project_name":cur_tree.root_node.label,
                                "area_type": doc.area_type,
                                "area_name":node.title

                                };
                                frappe.set_route("Form","Allocate Organization","new-allocate-organization-1");  
                                console.log(doc.area_type)

                            })

                            console.log("add_organization "+cur_tree.root_node.label);

                        }
                    },
                    {
                       "fieldname": "column_break_4",
                       "fieldtype": "Column Break"
                    },                   
                    {        
                        label: 'Allocate Product',
                        fieldname: 'allocate_product',
                        fieldtype: 'Button',
                        click:function(){
                            console.log("allocate_product");

                            frappe.db.get_doc('Area', node.title)
                            .then(doc => {
                                frappe.route_options = {
                                "project_name":cur_tree.root_node.label,
                                "area_type": doc.area_type,
                                "area_name":node.title

                                };
                                frappe.set_route("Form","Product","new-product-1");  
                                console.log(doc.area_type)

                            })
                        }
                    }, 
                    {
                       "fieldname": "column_break_5",
                       "fieldtype": "Column Break"
                    },                    
                    {
                        label: 'Material Purchase',
                        fieldname: 'material_purchase',
                        fieldtype: 'Button',
                        click:function(){
                            console.log("purchase ");

                            frappe.db.get_doc('Area', node.title)
                            .then(doc => {
                                frappe.route_options = {
                                "project_name":cur_tree.root_node.label,
                                "area_type": doc.area_type,
                                "area_name":node.title

                                };
                                frappe.set_route("Form","Purchase Order","new-purchase-order-1");  
                                console.log(doc.area_type)

                            })
                        }
                    },
                    {
                        "fieldname": "section_break_1",
                        "fieldtype": "Section Break"
                    },
                    {
                        label: 'Organization Report',
                        fieldname: 'organization_report',
                        fieldtype: 'Button',
                        click:function(){
                            console.log("organization_report");
                        }
                    },
                    {
                       "fieldname": "column_break_6",
                       "fieldtype": "Column Break"
                    },
                    {
                        label: 'Release Work Order',
                        fieldname: 'release_work_order',
                        fieldtype: 'Button',
                        click:function(){
                            console.log("Release Work Order");
                            frappe.db.get_doc('Area', node.title)
                                .then(doc => {
                                frappe.route_options = {
                                    "project_name":cur_tree.root_node.label,
                                    "area_type": doc.area_type,
                                    "area_name":node.title
                                    };
                                    frappe.set_route("Form","Work Order","new-work-order-1");  
                                    console.log(doc.area_type)

                            })
                        }
                    },
                    {
                       "fieldname": "column_break_7",
                       "fieldtype": "Column Break"
                    },
                    {
                        label: 'Release for Execution',
                        fieldname: 'release_for_execution',
                        fieldtype: 'Button',
                        click:function(){
                            console.log("release_for_execution");
                            frappe.db.get_doc('Area', node.title)
                                .then(doc => {
                                frappe.route_options = {
                                    "project_name":cur_tree.root_node.label,
                                    "area_type": doc.area_type,
                                    "area_name":node.title
                                    };
                                    frappe.set_route("Form","Attain Area SOP Task","new-attain-area-sop-task-1");  
                                    console.log(doc.area_type)

                            })
                        }
                    },                    
    
                ],
                primary_action_label: 'Cancle',
                primary_action(values) {
                    console.log(values);
                    d.hide();
                }
            });

            d.show();



            },
            btnClass: "hidden-xs"
        }
    ],
    extend_toolbar: true
};
