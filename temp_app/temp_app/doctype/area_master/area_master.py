# -*- coding: utf-8 -*-
# Copyright (c) 2021, bhavin and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class AreaMaster(Document):
	pass


@frappe.whitelist()
def get_projects():
	print("#################shree funtion called#####################")
	doc_list=frappe.get_list("Project Master")
	project_names=[]
	for doc in doc_list:
		temp=frappe.get_doc("Project Master",doc)
		project_names.append(temp.project_name)
	project_names.sort()
	return project_names