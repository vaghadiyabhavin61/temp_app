// Copyright (c) 2021, bhavin and contributors
// For license information, please see license.txt

frappe.ui.form.on('Area Master', {
	refresh: function(frm) {
		if(frm.doc.area_level=="Sub-area")
		{
			frm.set_df_property("master_area_name","hidden",0);
		}
		else{
			frm.set_df_property("master_area_name","hidden",1);	
		}

	},
	area_level:function(frm){
		if(frm.doc.area_level=="Sub-area")
		{
			frm.set_df_property("master_area_name","hidden",0);
		}
		else{
			frm.set_df_property("master_area_name","hidden",1);	
		}
	}

});
